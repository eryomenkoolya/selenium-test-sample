package testCases;

import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import pageObjects.LoginPage;
import pageObjects.LeadDetailPage;
import pageObjects.AddLeadPage;

public class TestLead {

	private WebDriver driver;
	private LoginPage loginPage;
	private AddLeadPage addLeadPage;
	private LeadDetailPage leadDetailPage;
	
	@BeforeClass
	@Parameters({"login_url", "base_url", "email", "password"})
	public void openLeadsPage(String login_url, String base_url, String email, String password){
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		addLeadPage = PageFactory.initElements(driver, AddLeadPage.class);
		leadDetailPage = PageFactory.initElements(driver, LeadDetailPage.class);
		
		driver.get(login_url);
		loginPage.loginAs(email, password);
		
		driver.get(base_url + "leads");
	}
	
	@Test
	@Parameters({"name", "surname", "company"})
	public void createLead(String name, String surname, String company){
		driver.findElement(By.id("leads-new")).click();
		addLeadPage.addLead(name, surname, company);
	}
	
	@Test
	@Parameters("note")
	public void createNote(String note) throws InterruptedException{
		leadDetailPage.addNote(note);
		Thread.sleep(3000);
		leadDetailPage.checkNoteVisibility(note);
	}
	
	@AfterClass
	public void closeBrowser(){
		driver.quit();
	}

}

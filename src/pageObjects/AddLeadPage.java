package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddLeadPage {
	
	@FindBy(id="lead-first-name")
	public WebElement firstName;
	
	@FindBy(id="lead-last-name")
	public WebElement lastName;
	
	@FindBy(id="lead-company-name")
	public WebElement companyName;
	
	@FindBy(css=".edit-buttons .btn-primary")
	public WebElement saveBttn;

	public void addLead(String name, String surname, String company){
		firstName.sendKeys(name);
		lastName.sendKeys(surname);
		companyName.sendKeys(company);
		saveBttn.click();
	}
}

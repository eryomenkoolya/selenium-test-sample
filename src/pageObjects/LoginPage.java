package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

	@FindBy(id="user_email")
	public WebElement emailField;
	
	@FindBy(id="user_password")
	public WebElement passwdField;
	
	@FindBy(css=".btn-primary")
	public WebElement logInBttn;
	
	public void loginAs(String email, String password){
		emailField.sendKeys(email);
		passwdField.sendKeys(password);
		logInBttn.click();
	}
}

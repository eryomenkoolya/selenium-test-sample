package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class LeadDetailPage {
	
	@FindBy(css="#updates textarea")
	public WebElement noteField;
	
	@FindBy(css="#updates button")
	public WebElement saveBttn;
	
	@FindBy(css="#middle .list-container")
	public WebElement activityList;

	public void addNote(String note){
		noteField.sendKeys(note);
		saveBttn.click();
	}
	
	public void checkNoteVisibility(String note){
		Assert.assertTrue(activityList.getText().contains(note));
	}
}
